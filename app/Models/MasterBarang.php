<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\TransaksiPembelianBarang;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MasterBarang extends Model
{
    use HasFactory;

    protected $table = "master_barang";
    protected $fillable = ['id','nama_barang','harga_satuan'];

    public function barang(){
        return $this->hasMany(TransaksiPembelianBarang::class);
    }
}
