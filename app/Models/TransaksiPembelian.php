<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\TransaksiPembelianBarang;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TransaksiPembelian extends Model
{
    use HasFactory;

    protected $table = "transaksi_pembelian";
    protected $fillable = ['total_harga'];

    public function barang(){
        return $this->hasMany(TransaksiPembelianBarang::class);
    }
}
