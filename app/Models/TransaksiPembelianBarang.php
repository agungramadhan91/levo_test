<?php

namespace App\Models;

use App\Models\MasterBarang;
use Illuminate\Database\Eloquent\Model;
use App\Models\TransaksiPembelianBarang;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TransaksiPembelianBarang extends Model
{
    use HasFactory;

    protected $table = "transaksi_pembelian_barang";
    protected $fillable = ['transaksi_pembelian_id','master_barang_id','jumlah','harga_satuan'];

    public function good(){
        return $this->belongsTo(MasterBarang::class);
    }

    public function transaksi(){
        return $this->belongsTo(TransaksiPembelianBarang::class);
    }
}
