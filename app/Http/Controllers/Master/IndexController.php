<?php

namespace App\Http\Controllers\Master;

use App\Models\MasterBarang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $master = MasterBarang::all();

        return view('dashboard', compact('master'));
    }
}
