<?php

namespace App\Http\Controllers\Master;

use App\Models\MasterBarang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $name = $request->name;
        $price = $request->price;

        MasterBarang::create([
            'nama_barang' => $name,
            'harga_satuan' => $price
        ]);

        return redirect()->back();
    }
}
