<?php

namespace App\Http\Controllers;

use App\Models\MasterBarang;
use Illuminate\Http\Request;
use App\Models\TransaksiPembelian;
use App\Models\TransaksiPembelianBarang;

class BuyStoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $req_master = $request->master;
        $req_qty = $request->qty;
        $req_price = $request->price;
        $req_total = $request->total;
        
        // return dd(count($req_master));
        $transaksi = TransaksiPembelian::create([
            'total_harga' => $req_total
        ]);

        for ($i=0; $i < count($req_master); $i++) { 
            $master = MasterBarang::find($req_master[$i]);
            $barang = TransaksiPembelianBarang::create([
                'transaksi_pembelian_id' => $transaksi->id,
                'master_barang_id' => $master->id,
                'jumlah'    => $req_qty[$i],
                'harga_satuan' => $master->harga_satuan
            ]);
        }
         

        return redirect()->back();
    }
}
