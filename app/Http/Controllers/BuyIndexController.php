<?php

namespace App\Http\Controllers;

use App\Models\MasterBarang;
use Illuminate\Http\Request;
use App\Models\TransaksiPembelian;
use App\Models\TransaksiPembelianBarang;

class BuyIndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $master = MasterBarang::all();
        $transaksi = TransaksiPembelian::all();
        $barang = TransaksiPembelianBarang::all();

        return view('buy', compact('transaksi','barang','master'));
    }
}
