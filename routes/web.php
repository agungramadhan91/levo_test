<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GetController;
use App\Http\Controllers\BuyIndexController;
use App\Http\Controllers\BuyStoreController;
use App\Http\Controllers\Master\IndexController;
use App\Http\Controllers\Master\StoreController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('dashboard',  IndexController::class)->name('master.index');
Route::post('master/store', StoreController::class)->name('master.store');
Route::get('master/{id}', GetController::class)->name('master.get');
Route::get('transaksi', BuyIndexController::class)->name('transaksi.index');
Route::post('transaksi/store', BuyStoreController::class)->name('transaksi.store');

require __DIR__.'/auth.php';
