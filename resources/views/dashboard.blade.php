@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card mb-4">
                <div class="card-header">
                    Input Master Barang
                </div>
                <div class="card-body">
                    <form class="user" action="{{ route('master.store') }}" method="post">
                        @csrf

                        <div class="form-group">
                            <input type="text" name="name" class="form-control form-control-user"
                                placeholder="Nama Barang">
                        </div>
                        <div class="form-group">
                            <input type="text" name="price" class="form-control form-control-user"
                                placeholder="Harga Satuan">
                        </div>
                        
                        <button type="submit" class="btn btn-primary btn-user btn-block">Simpan</button>
                        
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nama Barang</th>
                                    <th>Harga Satuan</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                @foreach ($master as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->nama_barang }}</td>
                                        <td>{{ $item->harga_satuan }}</td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection