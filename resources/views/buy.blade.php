@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-header">
                    Input Pemeblian Baru
                </div>
                <div class="card-body">
                    <form class="user" action="{{ route('transaksi.store') }}" method="post">
                        @csrf
    
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select name="master[]" class="form-control master" id="master">
                                        <option value="" selected>-- MASUKAN ID BARANG --</option>
            
                                        @foreach ($master as $item)
                                            <option value="{{ $item->id }}">{{ $item->id }} - {{ $item->nama_barang }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" name="qty[]" value="1" id="qty1" class="form-control qty"
                                        placeholder="Jumlah">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" name="price[]" id="price1" class="form-control price"
                                        placeholder="Harga">
                                    
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-primary">+</button>
                            </div>
                        </div>
                        <input type="hidden" name="total" id="total">
                        
                        <button type="submit" class="btn btn-primary btn-user btn-block">Simpan</button>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Transaksi</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nama Total</th>
                                    <th>Barang</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                @foreach ($transaksi as $trans)
                                    <tr>
                                        <td>{{ $trans->id }}</td>
                                        <td>{{ $trans->total_harga }}</td>
                                        <td>
                                            @foreach ($trans->barang as $barang)
                                                {{ $barang->good }},
                                            @endforeach
                                        </td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection