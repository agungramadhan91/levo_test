<?php

namespace Database\Seeders;

use App\Models\MasterBarang;
use Illuminate\Database\Seeder;

class MasterBarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterBarang::create(['id'=>1, 'nama_barang'=>'Sabun batang', 'harga_satuan'=>'3000']);
        MasterBarang::create(['id'=>2, 'nama_barang'=>'Mi instan', 'harga_satuan'=>'2000']);
        MasterBarang::create(['id'=>3, 'nama_barang'=>'Pensil', 'harga_satuan'=>'1000']);
        MasterBarang::create(['id'=>4, 'nama_barang'=>'Kopi sachet', 'harga_satuan'=>'1500']);
        MasterBarang::create(['id'=>5, 'nama_barang'=>'Air minum galon', 'harga_satuan'=>'20000']);
    }
}
